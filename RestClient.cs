using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web.Services.Protocols;
using System.Net;
using System.IO;

using BetfairNG.API.TransferObjects;

namespace BetfairNG.API
{
    internal class RestClient : HttpWebClientProtocol
    {
        public string EndPoint { get; private set; }
        private static readonly IDictionary<string, Type> operationReturnTypeMap = new Dictionary<string, Type>();
        public const string APPKEY_HEADER = "X-Application";
        public const string SESSION_TOKEN_HEADER = "X-Authentication";
        public NameValueCollection CustomHeaders { get; set; }
        private static readonly String LIST_EVENT_TYPES_METHOD = "listEventTypes";
        private static readonly String LIST_EVENTS_METHOD = "listEvents";
        private static readonly String LIST_MARKET_CATALOGUE_METHOD = "listMarketCatalogue";
        private static readonly String LIST_MARKET_BOOK_METHOD = "listMarketBook";
        private static readonly String PLACE_ORDERS_METHOD = "placeOrders";
        private static readonly String LIST_ORDERS_METHOD = "listCurrentOrders";
        private static readonly String CANCEL_ORDERS_METHOD = "cancelOrders";
        private static readonly String GET_ACCOUNT_FUNDS_METHOD = "getAccountFunds";
        private static readonly String GET_ACCOUNT_DETAILS_METHOD = "getAccountDetails";
        private static readonly String LIST_MARKET_PROFIT_AND_LOSS_METHOD = "listMarketProfitAndLoss";
        private static readonly String FILTER = "filter";
        private static readonly String LOCALE = "locale";
        private static readonly String CURRENCY_CODE = "currencyCode";
        private static readonly String MARKET_PROJECTION = "marketProjection";
        private static readonly String MATCH_PROJECTION = "matchProjection";
        private static readonly String ORDER_PROJECTION = "orderProjection";
        private static readonly String PRICE_PROJECTION = "priceProjection";
        private static readonly String SORT = "sort";
        private static readonly String SORT_DIR = "sortDir";
        private static readonly String MAX_RESULTS = "maxResults";
        private static readonly String MARKET_IDS = "marketIds";
        private static readonly String MARKET_ID = "marketId";
        private static readonly String BET_IDS = "betIds";
        private static readonly String DATE_RANGE = "dateRange";
        private static readonly String ORDER_BY = "orderBy";
        private static readonly String INSTRUCTIONS = "instructions";
        private static readonly String CUSTOMER_REFERENCE = "customerRef";
        private static readonly String FROM_RECORD = "fromRecord";
        private static readonly String RECORD_COUNT = "recordCount";
        private static readonly String PLACED_DATE_RANGE = "placedDateRange";
        private static readonly String INCLUDE_SETTLED_BETS = "includeSettledBets";
        private static readonly String INCLUDE_BSP_BETS = "includeBspBets";
        private static readonly String NET_OF_COMMISSION = "netOfCommission";
     
        public RestClient(string endPoint, string appKey, string sessionToken)
		{
            this.EndPoint = endPoint + "/rest/v1/";
            CustomHeaders = new NameValueCollection();
            if (appKey != null)
            {
                CustomHeaders[APPKEY_HEADER] = appKey;
            }
            if (sessionToken != null)
            {
                CustomHeaders[SESSION_TOKEN_HEADER] = sessionToken;
            }
		}

        public IList<EventTypeResult> ListEventTypes(MarketFilter marketFilter, string locale = null)
        {
            var args = new Dictionary<string, object>();
            args[FILTER] = marketFilter;
            args[LOCALE] = locale;
            return Invoke<List<EventTypeResult>>(LIST_EVENT_TYPES_METHOD, args);
        }

        public IList<EventResult> ListEvents(MarketFilter marketFilter, string locale = null)
        {
            var args = new Dictionary<string, object>();
            args[FILTER] = marketFilter;
            args[LOCALE] = locale;
            return Invoke<List<EventResult>>(LIST_EVENTS_METHOD, args);
        }

        public IList<MarketCatalogue> ListMarketCatalogue(MarketFilter marketFilter, ISet<MarketProjection> marketProjections, MarketSort marketSort, string maxResult = "1", string locale = null)
        {
            var args = new Dictionary<string, object>();
            args[FILTER] = marketFilter;
            args[MARKET_PROJECTION] = marketProjections;
            args[SORT] = marketSort;
            args[MAX_RESULTS] = maxResult;
            args[LOCALE] = locale;
            return Invoke<List<MarketCatalogue>>(LIST_MARKET_CATALOGUE_METHOD, args);
        }

        public IList<MarketBook> ListMarketBook(IList<string> marketIds, PriceProjection priceProjection, OrderProjection? orderProjection = null, MatchProjection? matchProjection = null, string currencyCode = null, string locale = null)
        {
            var args = new Dictionary<string, object>();
            args[MARKET_IDS] = marketIds;
            args[PRICE_PROJECTION] = priceProjection;
            args[ORDER_PROJECTION] = orderProjection;
            args[MATCH_PROJECTION] = matchProjection;
            args[LOCALE] = locale;
            args[CURRENCY_CODE] = currencyCode;
            return Invoke<List<MarketBook>>(LIST_MARKET_BOOK_METHOD, args);
        }

        public IList<MarketProfitAndLoss> listMarketProfitAndLoss(IList<string> marketIds, bool includeSettledBets = false, bool includeBspBets = false, bool netOfCommission = false)
        {
            var args = new Dictionary<string, object>();
            args[MARKET_IDS] = marketIds;
            args[INCLUDE_SETTLED_BETS] = includeSettledBets;
            args[INCLUDE_BSP_BETS] = includeBspBets;
            args[NET_OF_COMMISSION] = netOfCommission;
            return Invoke<List<MarketProfitAndLoss>>(LIST_MARKET_PROFIT_AND_LOSS_METHOD, args);
        }

        public PlaceExecutionReport PlaceOrders(string marketId, IList<PlaceInstruction> instructions, string customerRef, string locale = null)
        {
            var args = new Dictionary<string, object>();
            args[MARKET_ID] = marketId;
            args[INSTRUCTIONS] = instructions;
            args[CUSTOMER_REFERENCE] = customerRef;
            args[LOCALE] = locale;
            return Invoke<PlaceExecutionReport>(PLACE_ORDERS_METHOD, args);
        }

        public CurrentOrderSummaryReport ListOrders(
            ISet<string> betIds, ISet<string> marketIds, TimeRange placedDateRange, TimeRange dateRange,
            OrderBy orderBy, SortDir sortDir, int recordCount, int fromRecord = 0, OrderProjection? orderProjection = null)
        {
            var args = new Dictionary<string, object>();
            args[BET_IDS] = betIds;
            args[MARKET_IDS] = marketIds;
            args[ORDER_PROJECTION] = orderProjection;
            args[PLACED_DATE_RANGE] = placedDateRange;
            args[DATE_RANGE] = dateRange;
            args[ORDER_BY] = orderBy;
            args[SORT_DIR] = sortDir;
            args[FROM_RECORD] = fromRecord;
            args[RECORD_COUNT] = recordCount;
            return Invoke<CurrentOrderSummaryReport>(LIST_ORDERS_METHOD, args);
        }

        public CancelExecutionReport CancelOrders(string marketId, IList<CancelInstruction> instructions, string customerRef)
        {
            var args = new Dictionary<string, object>();
            args[MARKET_ID] = marketId;
            args[INSTRUCTIONS] = instructions;
            args[CUSTOMER_REFERENCE] = customerRef;
            return Invoke<CancelExecutionReport>(CANCEL_ORDERS_METHOD, args);
        }

        public AccountFundsResponse GetAccountFunds()
        {
            return Invoke<AccountFundsResponse>(GET_ACCOUNT_FUNDS_METHOD);
        }

        public AccountDetailsResponse GetAccountDetails()
        {
            return Invoke<AccountDetailsResponse>(GET_ACCOUNT_DETAILS_METHOD);
        }

        protected HttpWebRequest CreateWebRequest(String restEndPoint)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(restEndPoint);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = 0;
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.Accept = "application/json";
            request.Headers.Add(CustomHeaders);
            request.KeepAlive = true;
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            return request;
        }


        public T Invoke<T>(string method, IDictionary<string, object> args = null)
        {
            if (method == null)
                throw new ArgumentNullException("method");
            if (method.Length == 0)
                throw new ArgumentException(null, "method");

            var restEndpoint = EndPoint + method + "/";
            var request = CreateWebRequest(restEndpoint);

            var postData = JsonHelper.Serialize<IDictionary<string, object>>(args);// +"}";

            var bytes = Encoding.GetEncoding("UTF-8").GetBytes(postData);
            request.ContentLength = bytes.Length;

            using (Stream stream = request.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            using (HttpWebResponse response = (HttpWebResponse)GetWebResponse(request))
           
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                var jsonResponse = reader.ReadToEnd();
                if (response.StatusCode != HttpStatusCode.OK) {
                    throw RestException(JsonHelper.Deserialize<BetfairNG.API.TransferObjects.Exception>(jsonResponse));
                }
                return JsonHelper.Deserialize<T>(jsonResponse);
            }
        }

        private static System.Exception RestException(BetfairNG.API.TransferObjects.Exception ex)
        {
            var data = ex.Detail;
            var exceptionName = data.Property("exceptionname").Value.ToString();
            var exceptionData = data.Property(exceptionName).Value.ToString();
            return JsonHelper.Deserialize<APINGException>(exceptionData);
        }

    }
}
