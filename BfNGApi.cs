﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

using BetfairNG.API.TransferObjects;
using BetfairNG.API.MenuObjects;
using Newtonsoft.Json.Linq;

namespace BetfairNG.API
{
    public static class BfNGApi
    {
        private static NavigationDataNode CreateNavigationDataNode(string type, JObject obj)
        {
            NavigationDataNode node = null;
            switch (type)
            {
                case "GROUP":
                    GroupNode groupNode = new GroupNode();
                    groupNode.Id = obj["id"].ToString();
                    groupNode.Name = obj["name"].ToString();
                    groupNode.Type = type;
                    node = groupNode;
                    break;
                case "EVENT_TYPE":
                    EventTypeNode eventTypeNode = new EventTypeNode();
                    eventTypeNode.Id = obj["id"].ToString();
                    eventTypeNode.Name = obj["name"].ToString();
                    eventTypeNode.Type = type;
                    node = eventTypeNode;
                    break;
                case "EVENT":
                    EventNode eventNode = new EventNode();
                    eventNode.Id = obj["id"].ToString();
                    eventNode.Name = obj["name"].ToString();
                    eventNode.Type = type;
                    eventNode.CountryCode = obj["countryCode"].ToString();
                    node = eventNode;
                    break;
                case "RACE":
                    RaceNode raceNode = new RaceNode();
                    raceNode.Id = obj["id"].ToString();
                    raceNode.Name = obj["name"].ToString();
                    raceNode.Type = type;
                    raceNode.StartTime = obj["startTime"].ToString();
                    raceNode.Venue = obj["venue"].ToString();
                    raceNode.CountryCode = obj["countryCode"].ToString();
                    node = raceNode;
                    break;
                case "MARKET":
                    MarketNode marketNode = new MarketNode();
                    marketNode.Id = obj["id"].ToString();
                    marketNode.Name = obj["name"].ToString();
                    marketNode.Type = type;
                    marketNode.ExchangeId = obj["exchangeId"].ToString();
                    marketNode.MarketStartTime = obj["marketStartTime"].ToString();
                    marketNode.MarketType = obj["marketType"].ToString();
                    marketNode.NumberOfWinners = obj["numberOfWinners"].ToString();
                    node = marketNode;
                    break;
                default:
                    throw new ArgumentException("Unknown MenuNode type " + type);
            }
            return node;
        }

        private static void RecursiveFetch(NavigationDataNode parentNode, JObject obj)
        {
            if (obj.Property("children") != null)
            {
                foreach (JObject o in obj["children"])
                {
                    NavigationDataNode node = CreateNavigationDataNode(o["type"].ToString(), o);
                    RecursiveFetch(node, o);
                    parentNode.Nodes.Add(node);
                }
            }
        }

        public static void Authenticate(string appKey, string username, string password)
        {
            var postData = String.Format("username={0}&password={1}", username, password);
            var bytes = Encoding.GetEncoding("UTF-8").GetBytes(postData);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://identitysso.betfair.com/api/login");
            request.Method = "POST";
            request.UseDefaultCredentials = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bytes.Length;
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.Headers.Add("X-Application", appKey);
            request.Accept = "application/json";

            using (Stream stream = request.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        LoginResponse loginResponse = JsonHelper.Deserialize<LoginResponse>(reader.ReadToEnd());

                        if (response.StatusCode != HttpStatusCode.OK)
                            throw new BfNGException("Authentication failed with HTTP status code " + response.StatusCode);
                        if (loginResponse.Status != "SUCCESS")
                            throw new BfNGException("Authentication failed: " + loginResponse.Status);

                        BfNGApiAuthInfo authInfo = new BfNGApiAuthInfo();
                        authInfo.SessionToken = loginResponse.SessionToken;
                        authInfo.AppKey = loginResponse.Product;
                    }
                }
            }
        }

        public static void Logout(string appKey, string sessionToken)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://identitysso.betfair.com/api/logout");
            request.Method = "POST";
            request.UseDefaultCredentials = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = 0;
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.Headers.Add("X-Application", appKey);
            request.Headers.Add("X-Authentication", sessionToken);
            request.Accept = "application/json";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        LogoutResponse logoutResponse = JsonHelper.Deserialize<LogoutResponse>(reader.ReadToEnd());
                        if (response.StatusCode != HttpStatusCode.OK)
                            throw new BfNGException("Logout failed with HTTP status code " + response.StatusCode);
                        if (logoutResponse.Status != "SUCCESS")
                            throw new BfNGException("Logout failed: " + logoutResponse.Status);
                    }
                }
            }
        }

        public static void KeepAlive(string appKey, ref string sessionToken)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://identitysso.betfair.com/api/keepAlive");
            request.Method = "POST";
            request.UseDefaultCredentials = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = 0;
            request.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
            request.Headers.Add("X-Application", appKey);
            request.Headers.Add("X-Authentication", sessionToken);
            request.Accept = "application/json";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        KeepAliveResponse keepAliveResponse = JsonHelper.Deserialize<KeepAliveResponse>(reader.ReadToEnd());
                        if (response.StatusCode != HttpStatusCode.OK)
                            throw new BfNGException("KeepAlive failed with HTTP status code " + response.StatusCode);
                        if (keepAliveResponse.Status != "SUCCESS")
                            throw new BfNGException("KeepAlive failed: " + keepAliveResponse.Status);
                        sessionToken = keepAliveResponse.SessionToken;
                    }
                }
            }
        }

        public static NavigationData ListNavigationData(BfNGApiAuthInfo authInfo)
        {
            NavigationData menu = new NavigationData();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.betfair.com/exchange/betting/rest/v1/en/navigation/menu.json");
            request.Method = "GET";
            request.KeepAlive = true;
            request.Headers.Add("X-Application", authInfo.AppKey);
            request.Headers.Add("X-Authentication", authInfo.SessionToken);
            request.Accept = "application/json";
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        if (response.StatusCode != HttpStatusCode.OK)
                            throw new BfNGException("RetrieveMenu failed with HTTP status code " + response.StatusCode);

                        JObject obj = JObject.Parse(reader.ReadToEnd());
                        NavigationDataNode rootNode = CreateNavigationDataNode(obj["type"].ToString(), obj);
                        menu.Nodes.Add(rootNode);
                        RecursiveFetch(rootNode, obj);
                    }
                }
            }
            return menu;
        }


        public static IList<EventTypeResult> ListEventTypes(BfNGApiAuthInfo authInfo, 
            MarketFilter marketFilter, string locale = null)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/betting", authInfo.AppKey, authInfo.SessionToken);
            return client.ListEventTypes(marketFilter, locale);
        }

        public static IList<EventResult> ListEvents(BfNGApiAuthInfo authInfo, 
            MarketFilter marketFilter, string locale = null)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/betting", authInfo.AppKey, authInfo.SessionToken);
            return client.ListEvents(marketFilter, locale);
        }

        public static IList<MarketCatalogue> ListMarketCatalogue(BfNGApiAuthInfo authInfo, 
            MarketFilter marketFilter, ISet<MarketProjection> marketProjections, MarketSort marketSort, string maxResult = "1", string locale = null)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/betting", authInfo.AppKey, authInfo.SessionToken);
            return client.ListMarketCatalogue(marketFilter, marketProjections, marketSort, maxResult, locale);
        }

        public static IList<MarketBook> ListMarketBook(BfNGApiAuthInfo authInfo, 
            IList<string> marketIds, PriceProjection priceProjection, OrderProjection? orderProjection = null, MatchProjection? matchProjection = null, string currencyCode = null, string locale = null)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/betting", authInfo.AppKey, authInfo.SessionToken);
            return client.ListMarketBook(marketIds, priceProjection, orderProjection, matchProjection, currencyCode, locale);
        }

        public static IList<MarketProfitAndLoss> ListMarketProfitAndLoss(BfNGApiAuthInfo authInfo, 
            IList<string> marketIds, bool includeSettledBets = false, bool includeBspBets = false, bool netOfCommission = false)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/betting", authInfo.AppKey, authInfo.SessionToken);
            return client.listMarketProfitAndLoss(marketIds, includeSettledBets, includeBspBets, netOfCommission);
        }

        public static PlaceExecutionReport PlaceOrders(BfNGApiAuthInfo authInfo, 
            string marketId, IList<PlaceInstruction> instructions, string customerRef, string locale = null)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/betting", authInfo.AppKey, authInfo.SessionToken);
            return client.PlaceOrders(marketId, instructions, customerRef, locale);
        }

        public static CurrentOrderSummaryReport ListOrders(BfNGApiAuthInfo authInfo,
            ISet<string> betIds, ISet<string> marketIds, TimeRange placedDateRange, TimeRange dateRange, OrderBy orderBy, SortDir sortDir, int recordCount, int fromRecord = 0, OrderProjection? orderProjection = null)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/betting", authInfo.AppKey, authInfo.SessionToken);
            return client.ListOrders(betIds, marketIds, placedDateRange, dateRange, orderBy, sortDir, recordCount, fromRecord, orderProjection);
        }

        public static CancelExecutionReport CancelOrders(BfNGApiAuthInfo authInfo, 
            string marketId, IList<CancelInstruction> instructions, string customerRef)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/betting", authInfo.AppKey, authInfo.SessionToken);
            return client.CancelOrders(marketId, instructions, customerRef);
        }

        public static AccountFundsResponse GetAccountFunds(BfNGApiAuthInfo authInfo)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/account", authInfo.AppKey, authInfo.SessionToken);
            return client.GetAccountFunds();
        }

        public static AccountDetailsResponse GetAccountDetails(BfNGApiAuthInfo authInfo)
        {
            RestClient client = new RestClient("https://api.betfair.com/exchange/account", authInfo.AppKey, authInfo.SessionToken);
            return client.GetAccountDetails();
        }
    }
}
