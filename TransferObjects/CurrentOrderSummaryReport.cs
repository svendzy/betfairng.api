﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace BetfairNG.API.TransferObjects
{
    public class CurrentOrderSummaryReport
    {
        [JsonProperty(PropertyName = "currentOrders")]
        public List<CurrentOrderSummary> CurrentOrders { get; set; }

        [JsonProperty(PropertyName = "moreAvailable")]
        public bool MoreAvailable { get; set; }  
    }
}
