﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace BetfairNG.API.TransferObjects
{
    public class CancelExecutionReport
    {
        [JsonProperty(PropertyName = "customerRef")]
        public string CustomerRef { get; set; }

        [JsonProperty(PropertyName = "status")]
        public ExecutionReportStatus Status { get; set; }

        [JsonProperty(PropertyName = "errorCode")]
        public ExecutionReportErrorCode ErrorCode { get; set; }

        [JsonProperty(PropertyName = "marketId")]
        public string MarketId { get; set; }

        [JsonProperty(PropertyName = "instructionReports")]
        public List<CancelInstructionReport> InstructionReports { get; set; }
    }
}
