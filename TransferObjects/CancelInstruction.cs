using System;
using System.Collections.Generic;

using System.Text;
using Newtonsoft.Json;

namespace BetfairNG.API.TransferObjects
{
    public class CancelInstruction
    {
        [JsonProperty(PropertyName = "betId")]
        public string BetId { get; set; }

        [JsonProperty(PropertyName = "sizeReduction")]
        public double? SizeReduction { get; set; }
    }
}
