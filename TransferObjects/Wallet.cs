﻿using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BetfairNG.API.TransferObjects
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Wallet
    {
        UK,     //  The UK Exchange wallet
        AUSTRALIAN  // The Australian Exchange wallet
    }
}
