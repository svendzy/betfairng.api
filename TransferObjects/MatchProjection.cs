using System;
using System.Collections.Generic;

using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BetfairNG.API.TransferObjects
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MatchProjection
    {
        NO_ROLLUP, ROLLED_UP_BY_PRICE, ROLLED_UP_BY_AVG_PRICE
    }
}
