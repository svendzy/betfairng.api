﻿using System;

using Newtonsoft.Json;

namespace BetfairNG.API.TransferObjects
{
    public class AccountDetailsResponse
    {
        [JsonProperty(PropertyName = "currencyCode")]
        public string CurrencyCode { get; set; }        //  Default user currency Code.

        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }           //  First Name.

        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }            //  Last Name

        [JsonProperty(PropertyName = "localeCode")]
        public string LocaleCode { get; set; }          //  Locale Code

        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }              //  Region based on users zip/postcode (ISO 3166-1 alpha-3 format). Defaults to GBR if zip/postcode cannot be identified.

        [JsonProperty(PropertyName = "timezone")]
        public string Timezone { get; set; }            //  User Time Zone

        [JsonProperty(PropertyName = "discountRate")]
        public double DiscountRate { get; set; }        //  User Discount Rate

        [JsonProperty(PropertyName = "pointsBalance")]
        public int PointsBalance { get; set; }          //  The Betfair points balance

        [JsonProperty(PropertyName = "countryCode")]
        public string CountryCode { get; set; }         //  The customer's country of residence (ISO 2 Char format)
    }
}
