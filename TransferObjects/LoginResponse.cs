﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace BetfairNG.API.TransferObjects
{
    public class LoginResponse
    {
        [JsonProperty(PropertyName = "token")]
        public string SessionToken { get; set; }

        [JsonProperty(PropertyName = "product")]
        public string Product { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
    }
}
