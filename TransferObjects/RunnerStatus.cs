using System;
using System.Collections.Generic;

using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BetfairNG.API.TransferObjects
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RunnerStatus
    {
        ACTIVE, WINNER, LOSER, REMOVED_VACANT, REMOVED
    }
}
