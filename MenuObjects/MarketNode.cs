﻿using System;

namespace BetfairNG.API.MenuObjects
{
    public class MarketNode : NavigationDataNode
    {
        private string exchangeId;
        private string marketStartTime;
        private string marketType;
        private string numberOfWinners;

        public MarketNode() : base() { }

        public string ExchangeId
        {
            get { return exchangeId; }
            set { exchangeId = value; }
        }

        public string MarketStartTime
        {
            get { return marketStartTime; }
            set { marketStartTime = value; }
        }

        public string MarketType
        {
            get { return marketType; }
            set { marketType = value; }
        }

        public string NumberOfWinners
        {
            get { return numberOfWinners; }
            set { numberOfWinners = value; }
        }
    }
}
