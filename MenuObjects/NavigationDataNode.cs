﻿using System;
using System.Collections.Generic;

namespace BetfairNG.API.MenuObjects
{
    public abstract class NavigationDataNode
    {
        private string id;
        private string name;
        private string type;
        private List<NavigationDataNode> nodes;

        public NavigationDataNode()
        {
            nodes = new List<NavigationDataNode>();
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public List<NavigationDataNode> Nodes
        {
            get { return nodes; }
        }
    }
}
