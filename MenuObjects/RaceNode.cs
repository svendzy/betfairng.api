﻿using System;
using System.Collections.Generic;

namespace BetfairNG.API.MenuObjects
{
    public class RaceNode : NavigationDataNode
    {
        private string startTime;
        private string venue;
        private string countryCode;

        public RaceNode() : base() { }

        public string StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        public string Venue
        {
            get { return venue; }
            set { venue = value; }
        }

        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
    }
}
