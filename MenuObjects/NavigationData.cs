﻿using System;
using System.Collections.Generic;

namespace BetfairNG.API.MenuObjects
{
    public class NavigationData
    {
        List<NavigationDataNode> nodes;

        public NavigationData()
        {
            nodes = new List<NavigationDataNode>();
        }

        public List<NavigationDataNode> Nodes
        {
            get { return nodes; }
            set { nodes = value; }
        }
    }
}
