﻿using System;
using System.Collections.Generic;

namespace BetfairNG.API.MenuObjects
{
    public class EventNode : NavigationDataNode
    {
        private string countryCode;

        public EventNode() : base() { }

        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
    }
}
