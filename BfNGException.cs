﻿using System;

namespace BetfairNG.API
{
    public class BfNGException : System.Exception
    {
        public BfNGException()
        {
        }

        public BfNGException(string message)
            : base(message)
        {
        }

        public BfNGException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
