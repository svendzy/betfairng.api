﻿using System;

namespace BetfairNG.API
{
    public class BfNGApiAuthInfo
    {
        private string m_AppKey = String.Empty;
        private string m_SessionToken = String.Empty;

        public string AppKey
        {
            get { return m_AppKey; }
            set { m_AppKey = value; }
        }

        public string SessionToken
        {
            get { return m_SessionToken; }
            set { m_SessionToken = value; }
        }
    }
}
