using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BetfairNG.API
{
    class JsonHelper
    {
        public static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static JObject Parse(string json)
        {
            return JObject.Parse(json);
        }

        public static string Serialize<T>(T value)
		{
			var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore};
			return JsonConvert.SerializeObject(value, settings);
		}
    }
}
